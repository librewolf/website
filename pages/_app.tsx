import "../styles/globals.css";
import "nextra-theme-docs-patch-maltejur/style.css";

function MyApp({ Component, pageProps }) {
  return (
    <>
      {/* <center></center> */}
      <Component {...pageProps} />
      <style jsx>{`
        center {
          padding: 0.25em 0;
          background-color: light-dark(#ffdeb3, #3c2200);
        }

        :global(nav) {
          border-top-width: 1px;
          position: sticky !important;
        }
      `}</style>
    </>
  );
}

export default MyApp;

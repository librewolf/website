-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Contact: mailto:security-issues@librewolf.net
Expires: 2025-01-01T00:00:00.000Z
Encryption: https://keys.openpgp.org/search?q=892940311B95BCF8A6B25EED9CB760109F0C8D93
Preferred-Languages: en
Canonical: https://librewolf.net/.well-known/security.txt
Policy: https://librewolf.net/docs/security/
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEiSlAMRuVvPimsl7tnLdgEJ8MjZMFAmUO+L0ACgkQnLdgEJ8M
jZNF+hAAstL5w1WWll3UpbSAeKDuG0JhRkFScT5OQojjsM9+DARb12T/PWMRLsnz
cYbTbOJ1Hqa/umDcgrIVSQMDZi25KC54+hzqvvseN/XLubhcdLUD87vkj/Uo6U27
nLSqSHmVlkt9bwmtF+PXPSiv3ICZ09D1zYiztDm5WPkq7kvQ1v4TjMOg4x4wKshA
eoYTRFniv+AYq0J5PAZVXoyqSB2ZlfAFWaCd2wCjiNgKyTjKMcARAIL8UFwg5oAq
4U0KSNuYfh7eG1DLixlj2woVTmfEsGUGQB44pC7vLRKNxh0FSN2DDC4UP8KgcdT3
2O1o92OEUpwhT4yjq+goDGtIZM/jhFN3EDElpbS8sIU+T8Y2i7Z50Q4AhVE9/KvN
hM+NKN7mgcEA7jT7eOMlS8ULhr1DYg8atusYx1VnwsAuIUSQe10rrI7Q78ixVrcq
M9VINFhtYW8lrCqUMzf2Rr3Sbosh3k84zPs2YzOL4p4ZHCLcIVFohtnkcUTfERrv
7nx4H1clDn5il7g7TOIO8zrWurLSVnVzJySHfmsFvMbnd+LLh0wUr/98isPiEzm/
MucQsj6JT3fqQ4NeE6upf2kEp8r3S09rtUeD0dUiqTK5oHhoOyc9i1As99TC0AkV
CkyiWCy8k05Ym4vOe6ql4QaKHsEaBwi6jxE9S4G93jSUY+14w2w=
=IlZi
-----END PGP SIGNATURE-----
